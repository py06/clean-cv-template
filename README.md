# Introduction
I wanted to update my LaTeX resume (CV) lately, but I could not find any templates that suited my needs.
I found diferent interesting stuffs in multiple templates but in the end I wrote mine based
on my different findings. Now that it's good enough, I decided I will share it with everyone.


# Requirements & dependencies
The resume shall be built with **xelatex** (I did not test other method yet).

This resume makes use of roboto fonts and awesome fonts for icons.
If possible, install these font packages first using your distro package manager.
If you cannot find a matching package you can download archive from CTAN repo.
For ex:
- https://mirrors.ctan.org/install/fonts/roboto.tds.zip
- https://mirrors.ctan.org/fonts/fontawesome5.zip

To install, them you will have to perform a few steps that might differ from the shown steps here due, but it gives an idea of what should be done.

Go inside your local latex install directory (texmf) as root.
For example:

	sudo su -
	cd /usr/share/texmf
	wget https://mirrors.ctan.org/install/fonts/roboto.tds.zip
	unzip roboto.tds.zip
	texhash
	updmap-sys

# HOWTO
2 files are provided:
- clean_cv.cls : the classfile for the resume
- cv.tex : a dummy cv that you can use to create yours

In the cv.tex, you will see that you have to define a few variables and alsq
add few entries to complete your resume (a few **\\jobentry** for you work experience, some **\\qualificationentry**
for you education). In the end you will also have to fille a few **\\tableentry** describing you technical skillset, 
but also the different languages you speak and your extra-professional interesets.

When done, just run:

	xelatex cv.tex

It shall generate a pdf with you resume, et Voila!

# Output
Here is how it should look after building the dummy cv.

| Front page     |
| -------------- |
| <img src="./media/cv-1.png" width="200" >
