%------------------------------------------------------------------------------%
%  The MIT License (MIT)
%
%  Copyright (c) 2022 Pierre-Yves Kerbrat
%
%  Permission is hereby granted, free of charge, to any person obtaining a copy
%  of this software and associated documentation files (the "Software"), to deal
%  in the Software without restriction, including without limitation the rights
%  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
%  copies of the Software, and to permit persons to whom the Software is
%  furnished to do so, subject to the following conditions:
%
%  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
%  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
%  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
%  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
%  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
%  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
%  THE SOFTWARE.
%
%------------------------------------------------------------------------------%

\ProvidesClass{clean_cv}[2022/11/30 CV class]
\NeedsTeXFormat{LaTeX2e}
\DeclareOption{print}{\def\@cv@print{}}
\DeclareOption*{%
  \PassOptionsToClass{\CurrentOption}{article}
}
\ProcessOptions\relax
\LoadClass{article}

%----------------------------------------------------------------------------------------
%	ENCODING
%----------------------------------------------------------------------------------------

%we use utf8 since we want to build from any machine
%\usepackage[utf8]{inputenc}

%----------------------------------------------------------------------------------------
%	LOGIC
%----------------------------------------------------------------------------------------

% provides \isempty test
\usepackage{ifthen}

%----------------------------------------------------------------------------------------
%	FONTS
%----------------------------------------------------------------------------------------

% some tex-live fonts - choose your own
\usepackage{fontspec}
\usepackage[T1]{fontenc}
\usepackage{fontawesome5}
\usepackage{roboto}

\DeclareRobustCommand{\ulseries}{\fontseries{ul}\selectfont}
\DeclareRobustCommand{\lseries}{\fontseries{l}\selectfont}
\DeclareRobustCommand{\mseries}{\fontseries{m}\selectfont}
\DeclareRobustCommand{\mbseries}{\fontseries{mb}\selectfont}
\DeclareRobustCommand{\bseries}{\fontseries{b}\selectfont}
\DeclareRobustCommand{\ebseries}{\fontseries{eb}\selectfont}
\DeclareTextFontCommand{\textul}{\ulseries}
\DeclareTextFontCommand{\textl}{\lseries}
\DeclareTextFontCommand{\textm}{\mseries}
\DeclareTextFontCommand{\textmb}{\mbseries}
\DeclareTextFontCommand{\textb}{\bseries}
\DeclareTextFontCommand{\texteb}{\ebseries}

% set font default
\renewcommand*\familydefault{\sfdefault}

% more font size definitions
\usepackage{moresize}

\setmainfont{RobotoSlab}[
  Extension = .otf,
  UprightFont = *-Regular,
  %-- Upright --%
  FontFace={ul}{n}{Font=*-Thin},
  FontFace={l}{n}{Font=*-Light},
  FontFace={m}{n}{Font=*-Regular},
  FontFace={b}{n}{Font=*-Bold},
]

\setmonofont{RobotoMono}[
  Extension = .otf,
  UprightFont = *-Regular,
  %-- Upright --%
  FontFace={ul}{n}{Font=*-Thin},
  FontFace={l}{n}{Font=*-Light},
  FontFace={m}{n}{Font=*-Regular},
  FontFace={mb}{n}{Font=*-Medium},
  FontFace={b}{n}{Font=*-Bold},
  % %-- Italic --%
  FontFace={l}{it}{Font=*-LightItalic},
  FontFace={m}{it}{Font=*-Italic},
  FontFace={mb}{it}{Font=*-MediumItalic},
  FontFace={b}{it}{Font=*-BoldItalic},
]

\setsansfont{Roboto}[
  Extension = .otf,
  UprightFont = *-Regular,
  %-- Upright --%
  FontFace={ul}{n}{Font=*-Thin},
  FontFace={l}{n}{Font=*-Light},
  FontFace={m}{n}{Font=*-Regular},
  FontFace={mb}{n}{Font=*-Medium},
  FontFace={b}{n}{Font=*-Bold},
  FontFace={eb}{n}{Font=*-Black},
  % %-- Italic --%
  FontFace={ul}{it}{Font=*-ThinItalic},
  FontFace={l}{it}{Font=*-LightItalic},
  FontFace={m}{it}{Font=*-Italic},
  FontFace={mb}{it}{Font=*-MediumItalic},
  FontFace={b}{it}{Font=*-BoldItalic},
  FontFace={eb}{it}{Font=*-BlackItalic},
]


%----------------------------------------------------------------------------------------
%	COLORS DEFINITIONS
%----------------------------------------------------------------------------------------
% Color definitions
\usepackage[usenames,dvipsnames]{xcolor}
\definecolor{date}{HTML}{666666}
\definecolor{title}{HTML}{1D76E2}
\definecolor{headings}{HTML}{6A6A6A}
\definecolor{shade}{HTML}{e4e4e4} % Peach color for the contact information box
\definecolor{subheadings}{HTML}{333333}
\definecolor{linkcolor}{HTML}{641c1d} % 25% desaturated headings color for links
\definecolor{primarycolor}{HTML}{2b2b2b} % 25% desaturated headings color for links
\definecolor{altcolor}{HTML}{131388} % 25% desaturated headings color for links

%----------------------------------------------------------------------------------------
%       LINKS
%----------------------------------------------------------------------------------------

\usepackage{hyperref} % Required for links

\hypersetup{
        colorlinks=true, % Whether to color the text of links
        urlcolor=altcolor, % Color for \url and \href links
        linkcolor=altcolor, % Color for \nameref links
}
\urlstyle{same}

%----------------------------------------------------------------------------------------
%	PAGE LAYOUT  DEFINITIONS
%----------------------------------------------------------------------------------------

\usepackage{etoolbox}

%debug page outer frames
%\usepackage{showframe}

%define page styles using geometry
\usepackage[a4paper]{geometry}

% for example, change the margins to 2 inches all round
\geometry{top=1.75cm, bottom=1.75cm, left=1.5cm, right=1.5cm}

%----------------------------------------------------------------------------------------
% 	HEADER
%----------------------------------------------------------------------------------------

%use customized header
\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}

% remove top header line
\renewcommand{\headrulewidth}{0pt}
%remove botttom header line
\renewcommand{\footrulewidth}{0pt}
%remove pagenum
\renewcommand{\thepage}{}
%remove section num
\renewcommand{\thesection}{}

%less space between header and content
%\setlength{\headheight}{-5pt}

%customize entries left, center and right
\newcommand{\cvheader}[3]{
	\fancyhead[L,R]{}
	\fancyhead[C]{\sffamily\textul{#1 #2 $\cdot$ #3}}
	\fancyfoot[L,R]{}
	\fancyfoot[C]{\sffamily\textul{#1 #2 $\cdot$ #3}}
}

\usepackage{supertabular}
% parameters:
% 1 - firstname
% 2 - lastname
% 3 - jobtitle
% 4 - address
% 5 - email
% 6 - phone
% 7 - linkedin username
% 8 - git repo username
\newcommand{\contactsummary}[8]{

	\columnratio{0.55, 0.45} % Widths of the two columns, specified here as a ratio summing to 1 to correspond to percentages; adjust as needed for your content 
	\begin{paracol}{2} % Begin two-column mode

	\parbox[][0.11 \textheight][c]{\linewidth}{ % Box to hold your name and CV title; change the fixed height as needed to match the colored box to the right
		\centering % Horizontally center text
		\Huge \textcolor{primarycolor}{\textul{#1} #2} \\ % Your name
		%\medskip % Vertical whitespace
		{\LARGE\textcolor{altcolor}{#3}}
		\vfill % Push content to the top of the box
	}
	\switchcolumn % Switch to the second (right) column
	\parbox[top][0.11\textheight][c]{\linewidth}{ % Box to hold the colored box; change the fixed height as needed to match the box to the left
		\colorbox{shade}{ % Create colored box and specify background color
			\begin{supertabular}{@{\hspace{3pt}} p{0.05\linewidth} | p{0.775\linewidth}} % Start a table with two columns, the table will ensure everything is aligned
				\raisebox{-1pt}{\faHome} & #4 \\ % Address
				\raisebox{-1pt}{\faPhone} & #6  \\ % Phone number
				\raisebox{-1pt}{\small\faEnvelope} & \href{mailto:#5}{#5} \\ % Email address
				\raisebox{-1pt}{\faLinkedin} & \href{https://#7}{#7} \\ % LinkedIn profile
				\raisebox{-1pt}{\faGithub} & \href{https://#8}{#8} \\ % Git repo
			\end{supertabular}
		}
		\vfill % Push content to the top of the box
	}
	\end{paracol}
}

%indentation is zero
\setlength{\parindent}{0mm}

%----------------------------------------------------------------------------------------
%	TITLES
%----------------------------------------------------------------------------------------

\usepackage[nobottomtitles*]{titlesec} % Required for modifying sections, the nobottomtitles* pushes section titles to the next page when they are close to the bottom of the page

\renewcommand{\bottomtitlespace}{0.05\textheight} % The minimal space required from the bottom margin so a section title isn't moved to the next page

\titleformat
        {\section} % Section type being modified
        [block] % Section layout type, can be: hang, block, display, runin, leftmargin, rightmargin, drop, wrap, frame
        {\color{headings}\scshape\LARGE\raggedright} % Text formatting of the whole section, i.e. label and title
        {} % Section label (e.g. number) and its formatting
        {0pt} % Horizontal space between the section label and title
        {} % Code before the section title
        [\color{black}\titlerule] % Code after the section titl

\titlespacing*{\section}{0pt}{0pt}{8pt} % Spacing around section titles, the order is: left, before and after

%------------------------------------------------

\titleformat
        {\subsection} % Section type being modified
        [block] % Section layout type, can be: hang, block, display, runin, leftmargin, rightmargin, drop, wrap, frame
        {\itshape} % Text formatting of the whole section, i.e. label and title
        {} % Section label (e.g. number) and its formatting
        {0pt} % Horizontal space between the section label and title
        {} % Code before the section title
        [] % Code after the section title

\titlespacing*{\subsection}{0pt}{8pt}{3pt} % Spacing around section titles, the order is: left, before and after


%----------------------------------------------------------------------------------------
%	TABLE /ARRAY/LIST DEFINITIONS
%----------------------------------------------------------------------------------------

%for layouting tables
\usepackage{paracol}
\usepackage{enumitem}

%----------------------------------------------------------------------------------------
%       CUSTOM COMMANDS
%----------------------------------------------------------------------------------------

% Command for adding a new job entry (work experience)
\newcommand{\jobentry}[5]{
        {\raggedleft\textsc{#1\expandafter\ifstrequal\expandafter{#2}{}{}{\hspace{6pt}\footnotesize{(#2)}}}\par} % Duration and conditional full time/part time text
	\expandafter\ifstrequal\expandafter{#3}{}{}{{\raggedright\large \textcolor{altcolor}{#3}}\\} % Employer
        \expandafter\ifstrequal\expandafter{#4}{}{}{{\raggedright\large\textit{\textbf{#4}}}\\[4pt]} % Job title
        \expandafter\ifstrequal\expandafter{#5}{}{}{#5} % Description
        \medskip % Vertical whitespace
}

% Command for entering a new qualification (education)
\newcommand{\qualificationentry}[5]{
        \textsc{#1} & \textbf{#2}\\ % Duration and degree
        \expandafter\ifstrequal\expandafter{#3}{}{}{& {\small\textsc{#3}}\\} % Honors, achievements or distinctions (e.g. first class honors)
        \expandafter\ifstrequal\expandafter{#4}{}{}{& #4\\} % Department
        \expandafter\ifstrequal\expandafter{#5}{}{}{& \textit{#5}\\[5pt]} % Institution
}

% Command for entering a separate table row -- used for any section that uses a two column table for alignment
\newcommand{\tableentry}[3]{
        \textsc{#1} & #2\expandafter\ifstrequal\expandafter{#3}{}{\\}{\\[5pt]} % First the heading, then content, then a conditional insertion of whitespace if the third parameter has any content in it
}

%----------------------------------------------------------------------------------------
% CUSTOM STRUT FOR EMPTY BOXES
%----------------------------------------- -----------------------------------------------
\newcommand{\mystrut}{\rule[-.3\baselineskip]{0pt}{\baselineskip}}

%----------------------------------------------------------------------------------------
% SPREAD
%----------------------------------------------------------------------------------------
\newcommand{\spread}{7pt}

